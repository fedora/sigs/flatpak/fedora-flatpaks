# Fedora Flatpaks

This project is managed by the Fedora Flatpak SIG. It is primarily used for tracking issues related to Fedora's Flatpaks, including:

* Packaging issues with Flatpak apps
* Issues with the `fedora` Flatpak remote
* Tracking for apps to be made available as Flatpaks

See the [Fedora Flatpak SIG wiki page](https://fedoraproject.org/wiki/SIGs/Flatpak) for more information.
